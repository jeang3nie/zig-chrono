const std = @import("std");
const debug = std.debug;
const testing = std.testing;
const SECONDS_PER_DAY = @import("main.zig").SECONDS_PER_DAY;

pub const YearTag = enum(u1) {
    normal,
    leap,

    fn new(year: i32) YearTag {
        return if (@rem(year, 4) == 0 and (@rem(year, 100) != 0 or @rem(year, 400) == 0)) .leap else .normal;
    }
};

pub const Year = union(YearTag) {
    normal: i32,
    leap: i32,

    const Self = @This();

    pub fn new(year: i32) Self {
        return switch (YearTag.new(year)) {
            .normal => Self{ .normal = year },
            .leap => Self{ .leap = year },
        };
    }

    pub fn days(self: Self) u16 {
        return switch (self) {
            .normal => 365,
            .leap => 366,
        };
    }

    pub fn seconds(self: Self) i64 {
        return @as(i64, self.days()) * SECONDS_PER_DAY;
    }

    pub fn get(self: Self) i32 {
        return switch (self) {
            .normal => |year| year,
            .leap => |year| year,
        };
    }

    pub fn next(self: Self) Self {
        return Self.new(self.get() + 1);
    }

    pub fn previous(self: Self) Self {
        return Self.new(self.get() - 1);
    }

    pub fn format(
        self: Self,
        comptime fmt: []const u8,
        options: std.fmt.FormatOptions,
        writer: anytype,
    ) !void {
        _ = fmt;
        _ = options;

        const year = self.get();
        if (year > 0) {
            try writer.print("{d:0>4}", .{@as(u32, @intCast(year))});
        } else {
            try writer.print("{d:0>4}", .{year});
        }
    }
};
