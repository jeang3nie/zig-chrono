const std = @import("std");
const debug = std.debug;
const testing = std.testing;
const Year = @import("year.zig").Year;
const SECONDS_PER_DAY = @import("main.zig").SECONDS_PER_DAY;

pub const Month = enum(u4) {
    january = 1,
    february = 2,
    march = 3,
    april = 4,
    may = 5,
    june = 6,
    july = 7,
    august = 8,
    september = 9,
    october = 10,
    november = 11,
    december = 12,

    const Self = @This();

    pub fn days(self: Self, year: Year) u5 {
        return switch (@intFromEnum(self)) {
            1, 3, 5, 7, 8, 10, 12 => 31,
            2 => switch (year) {
                .normal => 28,
                .leap => 29,
            },
            else => 30,
        };
    }

    pub fn seconds(self: Self, year: Year) u32 {
        return @as(u32, self.days(year)) * SECONDS_PER_DAY;
    }

    pub fn next(self: Self) ?Self {
        const num = @intFromEnum(self);
        return if (num < 12) @enumFromInt(num + 1) else null;
    }

    pub fn previous(self: Self) ?Self {
        const num = @intFromEnum(self);
        return if (num > 1) @enumFromInt(num - 1) else null;
    }
};
